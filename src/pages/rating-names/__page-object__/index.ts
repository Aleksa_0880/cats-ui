import type { Page, TestFixture } from '@playwright/test';
import { test } from '@playwright/test';

export class RatingPage {
	private page: Page;
	public errorBlockSelector: string
	public ratingTableSelector: string
	public likeSelector: string

	constructor({
		page,
	}: {
		page: Page;
	}) {
		this.page = page;
		this.errorBlockSelector = '//div[contains(text(), "Ошибка загрузки рейтинга")]';
		this.ratingTableSelector = '.rating-names_table__Jr5Mf';
		this.likeSelector = '.rating-names_item-count__1LGDH.has-text-success';
	}


	async openRatingPage() {
		return await test.step('Открываю страницу рейтинга', async () => {
			await this.page.goto('/rating')
		})
	}

	async getLikeCounts() {
		const ratingTableLocator = this.page.locator(this.ratingTableSelector);
		const likeLocator = ratingTableLocator.locator(this.likeSelector);
		const likesCount = await likeLocator.allTextContents();
		return likesCount.map(text => parseInt(text, 10));
	}

	async checkLikes(likesList: number[]) {
		for (let i = 0; i < likesList.length - 1; i++) {
			if (likesList[i] < likesList[i + 1]) {
				throw new Error('Лайки не упорядочены по убыванию');
			}
		}
	}
}

export type RatingPageFixture = TestFixture<
	RatingPage,
	{
		page: Page;
	}
>;

export const ratingPageFixture: RatingPageFixture = async (
	{ page },
	use
) => {
	const raitingPage = new RatingPage({ page });

	await use(raitingPage);
};